#ifndef PERIPHERAL_H
#define PERIPHERAL_H
#include <stdint.h>

struct vector3D {
    int32_t X;
    int32_t Y;
    int32_t Z;
};

extern pthread_mutex_t i2c_mutex;

void *peripheral_thread(void *data);
void peripheral_thread_close(void);

#endif
