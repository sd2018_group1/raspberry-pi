#ifndef REMOTE_H
#define REMOTE_H
#include <stdint.h>

#define TANKBOT_STANDBY     0x00
#define TANKBOT_FOLLOW      0x01
#define TANKBOT_ATTACK      0x02
#define TANKBOT_RC          0x03

struct vector2D {
    int8_t X;
    int8_t Y;
};

#define TANKBOT_VECTOR (2 * sizeof(struct vector2D))
#define TANKBOT_CMD_DEFAULT {{0,0},{0,0},0,0,0}
struct __attribute__ ((__packed__)) TankBotCommand {
    struct vector2D moveDir;
    struct vector2D lookDir;
    uint8_t state;
    uint8_t fireState;
    uint8_t validCoordinates;    
};

extern struct TankBotCommand remote_data;

extern pthread_mutex_t remote_mutex;

void *remote_thread(void *data);
void remote_thread_close(void);

#endif
