#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <syslog.h>
#include "remote.h"

#define PORT 5000

pthread_mutex_t remote_mutex = PTHREAD_MUTEX_INITIALIZER;
int remote_thread_exit = 0;
struct TankBotCommand remote_data;
 
void *remote_thread(void *data) {
    struct sockaddr_in server_addr, client_addr;
    int client_len = sizeof(client_addr);
    struct TankBotCommand receive_cmd;
    int ret, sockID, bytesRead;

    sockID = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (sockID == -1) {
        fprintf(stderr, "Could not create socket: %s\n", strerror(errno));
        pthread_exit(&errno);
    }

    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);

    ret = bind(sockID, (struct sockaddr *)&server_addr, sizeof(server_addr));
    if (ret == -1) {
        fprintf(stderr, "Failed binding socket to port %d: %s\n"
                , PORT, strerror(errno));
        pthread_exit(&errno);
    }

    while (!remote_thread_exit) {

        memset(&receive_cmd, 0, sizeof(struct TankBotCommand));
        bytesRead = recvfrom(sockID, &receive_cmd, 
                sizeof(struct TankBotCommand), 0, 
                (struct sockaddr *)&client_addr, &client_len);
        if (bytesRead == -1) {
            fprintf(stderr, "Failed to read from connection: %s\n",
                   strerror(errno));
            pthread_exit(&errno);
        }
        if(memcmp(&remote_data, &receive_cmd, sizeof(struct TankBotCommand))){
            pthread_mutex_lock(&remote_mutex);
            remote_data = receive_cmd;
            pthread_mutex_unlock(&remote_mutex);
        }
    }
    close(sockID);
}

void remote_thread_close(void) {
    remote_thread_exit = 1;
}
