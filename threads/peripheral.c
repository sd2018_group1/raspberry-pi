#include <stdio.h>
#include <stdint.h>
#include <pthread.h>
#include <unistd.h>             //Used for UART
#include <fcntl.h>              //Used for UART
#include <termios.h>            //Used for UART
#include <errno.h>
#include <string.h>

/* UART definitions */
#define UART1_INTERFACE "/dev/ttyS0"

struct __attribute__ ((__packed__)) LIDARDATA {
    uint16_t Dist;
    uint8_t strength_L;
    uint8_t strength_H;
    uint8_t reserved;
    uint8_t OrigSigQuality;
    uint8_t checksum;
};

// threading variables
pthread_mutex_t peripheral_mutex = PTHREAD_MUTEX_INITIALIZER;
int peripheral_thread_exit = 0;

void *peripheral_thread(void *data) {
    int uart0_filestream = -1;
    struct LIDARDATA LidarData;
    struct termios options;
    uint8_t receive_data, checksum = 0xB2;  //0x59 + 0x59 required by checksum
    unsigned int valid_header = 0;
    unsigned int force_read = 0;
    unsigned int read_count = 0;

    /* start UART interface for LiDAR
     * Open in blocking read mode
     */
    uart0_filestream = open(UART1_INTERFACE, O_RDONLY | O_NOCTTY);
    if (uart0_filestream == -1)
        fprintf(stderr, "Error - Unable to open UART: %s\n", strerror(errno));

    /* UART options configure */
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;

    tcsetattr(uart0_filestream, TCSANOW, &options);
    tcflush(uart0_filestream, TCIOFLUSH);

    while (!peripheral_thread_exit) {
        read(uart0_filestream, &receive_data, 1);
        if(receive_data == 0x59)
            valid_header++;
        /* If last two bytes match valid header 
         * force read of next sizeof(struct LIDARDATA) bytes 
         */
        if(valid_header == 2){
            valid_header = 0;
            force_read = 1;
            continue;
        }
        /* Read next sizeof(struct LIDARDATA) bytes of the datastream 
         * and  create a checksum while we read
         */
        if(force_read){
            valid_header = 0;
            ((uint8_t *)&LidarData)[read_count++] = receive_data;
            
            if(read_count != sizeof(struct LIDARDATA))
                checksum += receive_data;
        }
        if(read_count == sizeof(struct LIDARDATA)){
            pthread_mutex_lock(&peripheral_mutex);
            // update global structs
            pthread_mutex_unlock(&peripheral_mutex);
            read_count = 0;
            force_read = 0;
            checksum = 0xB2;
        }
    }
    // thread exit
    close(uart0_filestream);
}

void peripheral_thread_close(void) {
    peripheral_thread_exit = 1;
}
