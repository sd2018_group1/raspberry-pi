#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>
#include <libgen.h>             /* For syslog */
#include <errno.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>           /* For mode constants */
#include <fcntl.h>              /* For O_* constants */
#include <unistd.h>
#include <sys/ioctl.h>
#include <signal.h>

#include <linux/spi/spidev.h>
#include "threads/remote.h"
#include "threads/peripheral.h"
#include "basedriver/basedriver.h"
#include "debug.h"
#include "bcm2837/bcm2837.h"

#define BD_RESET_PIN 34
#define BD_PWR_EN_PIN 35

#define SPI_DEVICE "/dev/spidev0.0"
#define SPI_SPEED 1000000
#define SPI_NORMALIZED_SPEED (SPI_SPEED*1.6)
#define SPI_BITS 8

static uint8_t mode = 3;
static uint8_t bits = SPI_BITS;
// raspberry pi divides speed by 1.6 so we multiply to compensate
static uint32_t speed = SPI_NORMALIZED_SPEED;

static int program_should_exit = 0;

void safe_str(char *str, int len){
    for (int i = 0; i < len; i++)
        if(str[i] < 0x20 || str[i] > 0x7F)
            str[i] = '.';
    str[len - 1] = '\0';
}

void intHandler(int s){
    printf("\nCaught signal %s\n",strsignal(s));
    program_should_exit = 1;
}

int main(int argc, char *argv[]) {
    pthread_t p_remote, p_peripheral;
    struct TankBotCommand master_state = TANKBOT_CMD_DEFAULT;
    struct bd_generic_cmd_t SPI_TX_cmd = BASEDRIVER_CMD_DEFAULT;
    struct bd_generic_cmd_t SPI_RX_cmd = BASEDRIVER_CMD_DEFAULT;
    struct bd_generic_cmd_t cmd = BASEDRIVER_CMD_DEFAULT;
    uint8_t oldState = 0xFF;
    int ret;
    int spi_fd;

    if (geteuid()) {
        fprintf(stderr, "This program requires super user permissions\n");
        return EXIT_FAILURE;
    }
    
    if (bcm2837_init()) 
        return EXIT_FAILURE;
    
    signal(SIGINT, intHandler);
   
    /* Claim and set GPIO for TIVA board power mosfet */
    bcm2837_gpio_set_pud(BD_PWR_EN_PIN, BCM2837_GPIO_PUD_OFF);
    bcm2837_gpio_fsel(BD_PWR_EN_PIN, BCM2837_GPIO_FSEL_OUTP);
    bcm2837_gpio_set(BD_PWR_EN_PIN);

    /* Set TIVA reset pin to output high, this enables the MCU */
    bcm2837_gpio_set_pud(BD_RESET_PIN, BCM2837_GPIO_PUD_OFF);
    bcm2837_gpio_fsel(BD_RESET_PIN, BCM2837_GPIO_FSEL_OUTP);
    bcm2837_gpio_set(BD_RESET_PIN);
    
    
    struct spi_ioc_transfer tr = {
        .tx_buf = (unsigned long)&SPI_TX_cmd,
        .rx_buf = (unsigned long)&SPI_RX_cmd,
        .len = sizeof(struct bd_generic_cmd_t),
        .delay_usecs = 0,
        .speed_hz = SPI_NORMALIZED_SPEED,
        .bits_per_word = SPI_BITS,
    };

    spi_fd = open(SPI_DEVICE, O_RDWR);
    if(spi_fd < 0){
        fprintf(stderr, "Could not open SPI device: %s\n",
               strerror(errno));
        goto spi_failure;
    }
    
    ret = ioctl(spi_fd, SPI_IOC_WR_MODE, &mode);
    if (ret == -1){
        fprintf(stderr, "can't set SPI mode %s\n", strerror(errno));
        goto spi_failure;
    }

    ret = ioctl(spi_fd, SPI_IOC_RD_MODE, &mode);
    if (ret == -1){
        fprintf(stderr, "can't get SPI mode %s\n",strerror(errno));
        goto spi_failure;
    }

    ret = ioctl(spi_fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (ret == -1){
        fprintf(stderr, "can't set bits per word %s\n",strerror(errno));;
        goto spi_failure;
    }

    ret = ioctl(spi_fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (ret == -1){
        fprintf(stderr, "can't get bits per word %s\n",strerror(errno));
        goto spi_failure;
    }

    ret = ioctl(spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1){
        fprintf(stderr, "can't set max speed hz %s\n",strerror(errno));
        goto spi_failure;
    }

    ret = ioctl(spi_fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (ret == -1){
        fprintf(stderr, "can't get max speed hz %s\n",strerror(errno));;
        goto spi_failure;
    }
    spi_failure:
    if(ret == -1){
        fprintf(stderr, "SPI error %s\n", strerror(errno));
        if(spi_fd > 0)
            close(spi_fd);
        return EXIT_FAILURE; 
    }
    debug_printf("SPI mode: %d\n", mode);
    debug_printf("bits per word: %d\n", bits);
    debug_printf("max speed: %d Hz (%d KHz)\n", SPI_SPEED, SPI_SPEED/1000);

    /* Start threads*/
    if (pthread_create(&p_peripheral, NULL, peripheral_thread, NULL)) {
        fprintf(stderr, "Could not create peripheral thread: %s\n",
               strerror(errno));
        return EXIT_FAILURE;
    }

    if (pthread_create(&p_remote, NULL, remote_thread, NULL)) {
        fprintf(stderr, "Could not create remote thread: %s\n",
               strerror(errno));
        return EXIT_FAILURE;
    }

    while (!program_should_exit) {
        pthread_mutex_lock(&remote_mutex);
        memcpy(&master_state, &remote_data, sizeof(struct TankBotCommand));
        pthread_mutex_unlock(&remote_mutex);
        switch (master_state.state){
            case TANKBOT_STANDBY:
                if(oldState != master_state.state){
                    oldState = master_state.state;
                    debug_printf("standby mode\n");
                }
                memset(&cmd, 0, sizeof(struct bd_generic_cmd_t));
                break;
            case TANKBOT_FOLLOW:
                if(oldState != master_state.state){
                    oldState = master_state.state;
                    debug_printf("follow mode\n");
                }
                cmd.type = BASEDRIVER_FOLLOW;
                cmd.fireState = 0;
                memcpy(&cmd.data, &master_state.moveDir, TANKBOT_VECTOR);
                break;
            case TANKBOT_ATTACK:
                if(oldState != master_state.state){
                    oldState = master_state.state;
                    debug_printf("attack mode\n");
                }
                cmd.type = BASEDRIVER_ATTACK;
                cmd.fireState = master_state.fireState;
                memcpy(&cmd.data, &master_state.moveDir, TANKBOT_VECTOR);
                break;
            case TANKBOT_RC:
                if(oldState != master_state.state){
                    oldState = master_state.state;
                    debug_printf("remote control mode\n");
                }
                cmd.type = BASEDRIVER_MOVE;
                cmd.fireState = master_state.fireState;
                memcpy(&cmd.data, &master_state.moveDir, TANKBOT_VECTOR);
                break;  
            default:
                if(oldState != master_state.state){
                    oldState = master_state.state;
                    fprintf(stderr, "Received unknown command: %d\n",
                            master_state.state);
                }
        }
        /* Send data to TIVA if data is new and clock in response if any*/
        if (!memcmp(&SPI_TX_cmd, &cmd, sizeof(struct bd_generic_cmd_t)))
            continue;
            
        memcpy(&SPI_TX_cmd, &cmd, sizeof(struct bd_generic_cmd_t));
        ret = ioctl(spi_fd, SPI_IOC_MESSAGE(1), &tr);
        if (ret < 1){
            fprintf(stderr, "Could not send SPI message (%d)\n", ret);
        }
        debug_printf("fire = %d\n", cmd.fireState);
        debug_printf("move %04d %04d\n", cmd.data[0], cmd.data[1]);
        debug_printf("look %04d %04d\n", cmd.data[2], cmd.data[3]);
        switch (SPI_RX_cmd.type){
            case BASEDRIVER_RESPONSE_NONE:
                break;
            case BASEDRIVER_RESPONSE_ERR:
                fprintf(stderr, "Received ERR response\n");
                break;
            case RX_BUF_OVERFLOW:
                fprintf(stderr, "Received RX_OVERFLOW err\n");
                break;
            default:
                fprintf(stderr, "Received unknown TX response: %d\n",
                        SPI_RX_cmd.type);
                break;
        }
    }

    program_exit:
    /* kill threads */
    remote_thread_close();
    peripheral_thread_close();

    if(spi_fd > 0)
        close(spi_fd);

    /* Clear all GPIO's */
    bcm2837_gpio_clr(BD_PWR_EN_PIN);
    bcm2837_gpio_fsel(BD_PWR_EN_PIN, BCM2837_GPIO_FSEL_INPT);
    bcm2837_gpio_clr(BD_RESET_PIN);
    bcm2837_gpio_fsel(BD_RESET_PIN, BCM2837_GPIO_FSEL_INPT);
    /* close mem and exit */
    bcm2837_close();
    exit(1);

    return EXIT_SUCCESS;
}
