#ifndef BASEDRIVER_H
#define BASEDRIVER_H

#include "../threads/remote.h"

/* BASEDRIVER RX commands*/
#define BASEDRIVER_RESPONSE_NONE    0x00
#define BASEDRIVER_RESPONSE_ERR     0xFF

#define RX_BUF_OVERFLOW             0x20

/* BASEDRIVER TX commands*/
#define BASEDRIVER_STOP             0x00
#define BASEDRIVER_MOVE             0x10
#define BASEDRIVER_FOLLOW           0x20
#define BASEDRIVER_ATTACK           0x30
#define TX_BUF_OVERFLOW             0x21

#define BASEDRIVER_CMD_DEFAULT      {0,0,{0,0,0,0}}
#define MAX_DATA_SIZE               4

struct __attribute__ ((__packed__)) bd_generic_cmd_t {
    uint8_t type;
    uint8_t fireState;
    int8_t data[MAX_DATA_SIZE];
};

struct __attribute__ ((__packed__)) bd_move_cmd_t {
    struct vector2D moveDir;
    struct vector2D lookDir;
};

#endif /* BASEDRIVER_H */

