#ifndef DEBUG_H
#define DEBUG_H
#include <stdio.h>

#define TANKBOT_DEBUG

#ifdef TANKBOT_DEBUG
#define debug_printf(x,...) printf(x, ##__VA_ARGS__)
#else
#define debug_printf(x,...)
#endif

#endif /* DEBUG_H */

